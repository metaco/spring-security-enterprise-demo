# Spring Security/Spring Security OAuth 核心原理

## Spring Security 核心过滤器链

可控过滤器：

- UsernamePasswordAuthenticationFilter
- BasicAuthenticationFilter

``每个过滤器的都将请求中的数据，尝试去做认证操作，没有成功， 则传递至下个过滤器``

不可控过滤器：

- **FilterSecurityInterceptor** (最后一个过滤器)
``FilterSecurityInterceptor 依据 SecurityConfig的配置，是否经过身份认证`` 最终决定是否能否访问
服务，（如果没有通过则会抛出不同的异常）

- **ExceptionTranslationFilter** (FilterSecurityInterceptor 前一个过滤器)
``用于处理 在其身后的 FilterSecurityInterceptor 所抛出的异常``

## Spring Security 核心服务

UserDetailService 加载用户信息。构建出一个 UserDetails 对象

- 默认使用 BCryptPasswordEncoder ，
Spring Security 默认生成一个 随机的盐串，在生成密码串后将盐混入其中
每次判断是使用 随机生成的盐，反推加密的串是什么，可以密码被泄露破解

- 认证流程
- 自定义 登录页面，自定义登录请求路径

根据登录的请求类型，判断是否是由 html 引发的 请求跳转
    - 否则返回一个 401 状态码
    - 和错误信息
- 认证成功处理， 失败处理流程


## 认证核心流程

- 1. **UsernamePasswordAuthenticationFilter**


- 2. **AuthenticationManager (本身不管理，认证相关逻辑)**
    
- 3. **AuthenticationProvider （校验逻辑）**
- 4. **UserDetailsService**
                              
- 5.    **UserDetails**
    
        -->  **Authentication** 

        -->  **preCheck , postCheck, authenticated(true)**
        
        -->  **onAuthenticationSuccess (/Failure)**
        
        -->  **handleAuthenticationException**
        
        -->  **SecurityContext** 
        
        ->  **SecurityContextHodler**

        -->  **SecurityContextPresistenceFilter (最前端过滤器)**
                            
## 如何将认证结果在多个请求间共享

Spring Security 何时在 session 放入 认证相关信息
    何时在 session 中读取数据

- **``SecurityContextPresistenceFilter (在所有过滤器之前)``**

工作原理： 
1. 每次请求中，检查session是否包含 ``SecurityContext``
    如果存在，则将 SecurityContext 放入线程``(ThreadLocal)``中
2. 当每次请求结束，经过最开始的 ``SecurityContextPresistenceFilter`` 时
检查 线程中是否存在 SecurityContext ,如果存在则放到session 中

## 如何获取认证信息


## JWT 特点

- 自包含

- 秘签

jwt 带有的签名是需要秘钥，来进行校验

- 可扩展


---

## 分布式登录态管理

- 会话有效期：

会话存储：cookie_id (user_id, expire_time)
        redis + mysql（降级） 分布式会话管理不可用
        redis-cluster

    tomcat 默认会话有效期 30分钟 （不与服务端发生交互行为的 呆滞时间）
    解决方案：续命Tomcat 
                增长 redis 有效时间到30分钟
- 会话续命

- 安全性管理
    session_id 安全性管理 
    header 头加入 token  (仍然不够安全)
    https: 只负责在传输过程中的数据加密
        （没有办法解决报文协议在客户端明文窃取）
    使用自定义协议 App 内部，协议数据传输 加密


## Spring Security OAuth 核心原理


### 核心过滤器

    org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter 
    org.springframework.security.web.context.SecurityContextPersistenceFilter 
    org.springframework.security.web.header.HeaderWriterFilter 
    org.springframework.security.web.authentication.logout.LogoutFilter 
    org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter 
    org.springframework.security.web.savedrequest.RequestCacheAwareFilter 
    org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter 
    org.springframework.security.web.authentication.AnonymousAuthenticationFilter 
    org.springframework.security.web.session.SessionManagementFilter
    org.springframework.security.web.access.ExceptionTranslationFilter
    org.springframework.security.web.access.intercept.FilterSecurityInterceptor



## 网关限流的缺陷

1. A服务网关转发限流100请求，并且B服务网关限流100请求，
2. 但是A服务会内部调用B服务，测试B服务流量达到200 ,B 服务崩溃，造成服务降级


## 认证头信息经过网关专递的缺陷

1. 认证效率低， 每次都需要去认证服务器请求认证信息
2. 专递认证信息，直接有网关向后传递（明文传输）
3. 服务之间传递认证信息，比较麻烦，需要向请求头放置数据
4. 细粒度的权限交由网关控制，（约占比90%） 微服务之间的权限只做黑白名单限制


## 自定义实现 OAuth2 认证授权模式 （手机验证码模式）

- 实现 TokenGranter
- 实现 AuthorizationServerTokenServices (默认实现)
- 实现 OAuth2RequestFactory (默认实现)
- 继承实现 AbstractAuthenticationToken （认证主体）
- 实现 AuthenticationProvider

