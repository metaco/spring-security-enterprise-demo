package club.example.gateway.service;

import club.example.gateway.domain.AuditLog;
import club.example.gateway.handler.AuditLogContextHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class OAuthUserPermissionServiceImpl implements OAuthUserPermissionService {

    private final GatewayAuditLogService gatewayAuditLogService;

    public OAuthUserPermissionServiceImpl(GatewayAuditLogService gatewayAuditLogService) {
        this.gatewayAuditLogService = gatewayAuditLogService;
    }

    @Override
    public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
        log.info("hasPermission requestURI = {}", request.getRequestURI());
        log.info(ReflectionToStringBuilder.toString(authentication));

        // 匿名用户不需要授权
        if (authentication instanceof AnonymousAuthenticationToken) {
            throw new AccessTokenRequiredException(null);
        }
        // 认证成功，授权判定，（此处为授权鉴定，非认证鉴定）
        AuditLog.AuditLogged auditLogged = AuditLogContextHandler.getContext();
        // request.setAttribute("accessed", true);
        boolean permissible = RandomUtils.nextInt() % 2 == 0;
        if (permissible && null != auditLogged) {
            gatewayAuditLogService.update(auditLogged.getRequestId(), HttpStatus.OK.value());
            auditLogged.setUpdated(true);
        }
        return permissible;
    }
}
