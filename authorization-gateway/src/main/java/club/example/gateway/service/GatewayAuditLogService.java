package club.example.gateway.service;

import club.example.gateway.domain.AuditLog;

import javax.servlet.http.HttpServletRequest;

public interface GatewayAuditLogService {

    AuditLog create(HttpServletRequest request, String principal);

    void create(HttpServletRequest request, String principal, int responseStatus);

    void update(String requestId, int responseStatus);
}
