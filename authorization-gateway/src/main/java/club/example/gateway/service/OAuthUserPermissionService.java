package club.example.gateway.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface OAuthUserPermissionService {

    boolean hasPermission(HttpServletRequest request, Authentication authentication);
}
