package club.example.gateway.service;
import java.time.LocalDateTime;
import java.util.UUID;

import club.example.gateway.domain.AuditLog;
import club.example.gateway.repository.AuditLogMapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class GatewayAuditLogServiceImpl implements GatewayAuditLogService {

    private final AuditLogMapper auditLogMapper;

    public GatewayAuditLogServiceImpl(AuditLogMapper auditLogMapper) {
        this.auditLogMapper = auditLogMapper;
    }

    @Override
    public AuditLog create(HttpServletRequest request, String principal) {
        String requestURI = request.getRequestURI();
        AuditLog auditLog = new AuditLog();

        auditLog.setPath(requestURI);
        auditLog.setRequestId(UUID.randomUUID().toString());
        auditLog.setRequestMethod(request.getMethod());
        auditLog.setResponseStatus(0);
        auditLog.setUsername(principal);
        auditLog.setCreateTime(LocalDateTime.now());
        auditLog.setUpdateTime(LocalDateTime.now());

        auditLogMapper.insert(auditLog);

        return auditLog;
    }

    @Override
    public void create(HttpServletRequest request, String principal, int responseStatus) {
        String requestURI = request.getRequestURI();
        AuditLog auditLog = new AuditLog();

        auditLog.setPath(requestURI);
        auditLog.setRequestId(UUID.randomUUID().toString());
        auditLog.setRequestMethod(request.getMethod());
        auditLog.setResponseStatus(responseStatus);
        auditLog.setUsername(principal);
        auditLog.setCreateTime(LocalDateTime.now());
        auditLog.setUpdateTime(LocalDateTime.now());

        auditLogMapper.insert(auditLog);

    }

    @Override
    public void update(String requestId, int responseStatus) {

        AuditLog auditLog = new AuditLog();
        auditLog.setResponseStatus(responseStatus);
        auditLog.setUpdateTime(LocalDateTime.now());
        LambdaUpdateWrapper<AuditLog> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(AuditLog::getRequestId, requestId);
        int updatedRows = auditLogMapper.update(auditLog, updateWrapper);

    }
}
