package club.example.gateway.configure;

import club.example.gateway.filter.GatewayAuditLogFilter;
import club.example.gateway.handler.GatewayAccessDeniedHandler;
import club.example.gateway.handler.GatewayAuthenticationEntryPoint;
import club.example.gateway.handler.GatewayExpressionHandler;
import club.example.gateway.service.GatewayAuditLogService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.access.ExceptionTranslationFilter;

/**
 * TODO 将网关变为资源服务器，在服务器启动时，
 *      获取认证服务器的JWT_SIGN_KEY, 读取JWT时需要使用kEY 验证签名
 *
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfigure extends ResourceServerConfigurerAdapter {

    private final GatewayExpressionHandler gatewayExpressionHandler;

    private final GatewayAccessDeniedHandler accessDeniedHandler;

    private final GatewayAuthenticationEntryPoint authenticationEntryPoint;

    private final GatewayAuditLogService gatewayAuditLogService;

    public ResourceServerConfigure(GatewayExpressionHandler gatewayExpressionHandler,
                                   GatewayAccessDeniedHandler accessDeniedHandler,
                                   GatewayAuthenticationEntryPoint authenticationEntryPoint,
                                   GatewayAuditLogService gatewayAuditLogService) {
        this.gatewayExpressionHandler = gatewayExpressionHandler;
        this.accessDeniedHandler = accessDeniedHandler;
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.gatewayAuditLogService = gatewayAuditLogService;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources
                .resourceId("gateway-resource")
                // 处理 HTTP 401 UnAuthorization 异常
                .authenticationEntryPoint(authenticationEntryPoint)
                // 处理 HTTP 403 Forbidden 异常
                .accessDeniedHandler(accessDeniedHandler)
                .expressionHandler(gatewayExpressionHandler);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // TODO 获取令牌的请求 /auth 网关前缀 (相对网关来说，直接放行）其他都需要身份验证
        // 将审计过滤器 加入 授权过滤器前面
        // TODO  授权过滤器，进行授权操作，如果失败一定会抛出异常信息，
        //       该过滤器会处理抛出的异常信息
        //
        http// .addFilterBefore(new GatewayRateLimitFilter(), SecurityContextPersistenceFilter.class)
                .addFilterBefore(new GatewayAuditLogFilter(gatewayAuditLogService), ExceptionTranslationFilter.class)
                .authorizeRequests()
                .antMatchers("/auth/**").permitAll()
                .anyRequest().access("#permissionService.hasPermission(request, authentication)");
    }
}
