package club.example.gateway.filter;

import club.example.gateway.domain.AuditLog;
import club.example.gateway.handler.AuditLogContextHandler;
import club.example.gateway.service.GatewayAuditLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * TODO 该过滤器 在 认证之后操作之后，授权操作之前
 *      审计过滤器
 */
// FIXME 这里不能把过滤器加入 Spring Bean, 不能保证顺序
@Slf4j
public class GatewayAuditLogFilter extends OncePerRequestFilter {

    private final GatewayAuditLogService gatewayAuditLogService;

    public GatewayAuditLogFilter(GatewayAuditLogService gatewayAuditLogService) {
        this.gatewayAuditLogService = gatewayAuditLogService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // 申请令牌时的用户名
        String principal = (String) authentication.getPrincipal();

        AuditLog auditLog = gatewayAuditLogService.create(request, principal);

        AuditLog.AuditLogged auditLogged = new AuditLog.AuditLogged();
        auditLogged.setRequestId(auditLog.getRequestId());
        auditLogged.setUpdated(false);
        AuditLogContextHandler.setContext(auditLogged);

        filterChain.doFilter(request, response);


        AuditLog.AuditLogged context = AuditLogContextHandler.getContext();
        if (null != context && !context.isUpdated()) {
                gatewayAuditLogService.update(
                        context.getRequestId(), HttpStatus.OK.value());
                AuditLogContextHandler.clearContext();
        }
    }
}
