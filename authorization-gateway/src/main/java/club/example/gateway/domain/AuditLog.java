package club.example.gateway.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@TableName("enterprise_audit_log")
public class AuditLog {

    @TableId
    private Long auditId;

    private String requestId;

    private String path;

    private String requestMethod;

    private Integer responseStatus;

    private String username;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    @Setter
    @Getter
    public static class AuditLogged {
        private String requestId;

        private boolean updated;
    }

}
