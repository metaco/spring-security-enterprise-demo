package club.example.gateway.handler;

import club.example.core.JsonResponse;
import club.example.gateway.domain.AuditLog;
import club.example.gateway.service.GatewayAuditLogService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 处理异常信息
 */
@Slf4j
@Component
public class GatewayAccessDeniedHandler extends OAuth2AccessDeniedHandler {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final GatewayAuditLogService auditLogService;

    public GatewayAccessDeniedHandler(GatewayAuditLogService auditLogService) {
        this.auditLogService = auditLogService;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException authException) throws IOException, ServletException {
        // log.info("audit log update log access denied");
        AuditLog.AuditLogged auditLogged = AuditLogContextHandler.getContext();
        if (null != auditLogged) {
            auditLogService.update(auditLogged.getRequestId(), HttpStatus.FORBIDDEN.value());
            AuditLogContextHandler.clearContext();
        }
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(
                objectMapper.writeValueAsString(new JsonResponse<>(-1000,
                        authException.getMessage()))
        );
    }
}
