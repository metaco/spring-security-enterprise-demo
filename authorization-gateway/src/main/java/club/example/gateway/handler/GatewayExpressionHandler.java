package club.example.gateway.handler;

import club.example.gateway.service.OAuthUserPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;

/**
 *  Spring 自定义权限表达式配置
 */
@Component
public class GatewayExpressionHandler extends OAuth2WebSecurityExpressionHandler {

    private final OAuthUserPermissionService permissionService;

    @Autowired
    public GatewayExpressionHandler(OAuthUserPermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @Override
    protected StandardEvaluationContext createEvaluationContextInternal(Authentication authentication, FilterInvocation invocation) {
        StandardEvaluationContext evaluationContext =
                super.createEvaluationContextInternal(authentication, invocation);
        evaluationContext.setVariable("permissionService", permissionService);
        return evaluationContext;
    }
}
