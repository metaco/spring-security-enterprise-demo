package club.example.gateway.handler;


import club.example.gateway.domain.AuditLog;

public class AuditLogContextHandler {

    private static final ThreadLocal<AuditLog.AuditLogged> contextHolder = new ThreadLocal<>();

    public static void clearContext() {
        contextHolder.remove();
    }

    public static AuditLog.AuditLogged getContext() {
        return contextHolder.get();
    }

    public static void setContext(AuditLog.AuditLogged context) {
        contextHolder.set(context);
    }
}
