package club.example.gateway.handler;

import club.example.core.JsonResponse;
import club.example.gateway.domain.AuditLog;
import club.example.gateway.service.GatewayAuditLogService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * If authentication fails and the caller has asked for a specific content type response,
 *  this entry point can send one,
 *  * along with a standard 401 status.
 */
@Component
public class GatewayAuthenticationEntryPoint extends OAuth2AuthenticationEntryPoint {

    private final GatewayAuditLogService gatewayAuditLogService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public GatewayAuthenticationEntryPoint(GatewayAuditLogService gatewayAuditLogService) {
        this.gatewayAuditLogService = gatewayAuditLogService;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AuditLog.AuditLogged auditLogged = AuditLogContextHandler.getContext();

        if (null == authentication) {
            if (auditLogged != null) {
                gatewayAuditLogService.update(auditLogged.getRequestId(),
                        HttpStatus.UNAUTHORIZED.value());
            }else {
                gatewayAuditLogService.create(request, "anonymousUser",
                        HttpStatus.UNAUTHORIZED.value());
            }
        } else {
            if (authException instanceof AccessTokenRequiredException) {
                gatewayAuditLogService.create(request, (String) authentication.getPrincipal(),
                        HttpStatus.UNAUTHORIZED.value());
            }
        }

        AuditLogContextHandler.clearContext();

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(objectMapper.writeValueAsString(new JsonResponse<>(-1003,
                authException.getMessage())));
    }
}
