package club.example.resource.controller;

import club.example.core.JsonResponse;
import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/service")
public class ResourceApplicationController {

    @GetMapping
    public JsonResponse<String> getResourceOwner(@AuthenticationPrincipal String username) {
        log.info("getResourceOwner username = {}", username);
        return new JsonResponse<>(0, "success", username);
    }

    @GetMapping("/role")
    @SentinelResource(value = "resourceRole", blockHandler = "doOnBlockMethod")
    public JsonResponse<String> getResourceRole(@AuthenticationPrincipal String username) {
        log.info("getResourceRole username : {}", username);
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 声明一个 资源
//        try (Entry entry = SphU.entry("resourceRole")) {
//            long createTime = entry.getCreateTime();
//            log.info("username = {}, createTime = {}", username, createTime);
//        }  catch (BlockException e) {
//            // 限流触发
//            log.error("blocked! message = {}", e.getMessage());
//
//        }
        return new JsonResponse<>(0, "success", "resourceRole");
    }


    public JsonResponse<String> doOnBlockMethod(@AuthenticationPrincipal String username,
                                                BlockException exception) {
        log.warn("blocked by {}", exception.getClass().getSimpleName());
        return new JsonResponse<>(-10023, "blocked error");
    }

    @GetMapping("/{id}")
    @SentinelResource("resourcePath")
    public JsonResponse<String> getResourcePath(@PathVariable("id") long id, @AuthenticationPrincipal String username) {
        log.info("resource id = {}", id);
        log.info("username = {}", username);
        return new JsonResponse<>(0, "success", "resourcePath Id = " + id);
    }
}
