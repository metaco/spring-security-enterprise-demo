package club.example.resource.configure;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Collections;

// TODO Spring 所有的Bean 组装完成后，发出该Event (等于系统启动完成后）

@Component
public class SentinelConfigure implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        FlowRule rule = new FlowRule();
        rule.setResource("resourceRole");
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);  // 流控规则按照 QPS 做限制
        // QPS 计数为 1
        rule.setCount(10);
        // 加入 RuleManager
        FlowRuleManager.loadRules(Collections.singletonList(rule));

        DegradeRule degradeRule = new DegradeRule();
        degradeRule.setResource("resourceRole");
        degradeRule.setGrade(RuleConstant.DEGRADE_GRADE_RT);
        degradeRule.setCount(10);       // 响应时间超过 10ms
        degradeRule.setTimeWindow(10);  // 设置 熔断器 打开/关闭时间窗口

        DegradeRuleManager.loadRules(Collections.singletonList(degradeRule));

    }
}
