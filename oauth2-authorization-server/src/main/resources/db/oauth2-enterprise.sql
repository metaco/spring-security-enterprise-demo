-- MySQL dump 10.13  Distrib 5.7.25, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: security-enterprise
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `enterprise_audit_log`
--

DROP TABLE IF EXISTS `enterprise_audit_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_audit_log`
(
    `audit_id`        bigint(20)           NOT NULL AUTO_INCREMENT COMMENT '审计ID',
    `request_id`      varchar(64) DEFAULT '',
    `path`            varchar(64)          NOT NULL COMMENT '请求URI',
    `request_method`  varchar(8)           NOT NULL COMMENT '请求方法',
    `response_status` smallint(3) unsigned NOT NULL COMMENT '响应状态码',
    `username`        varchar(32)          NOT NULL COMMENT '请求用户名',
    `create_time`     datetime             NOT NULL COMMENT '创建时间',
    `update_time`     datetime             NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`audit_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise_audit_log`
--

LOCK TABLES `enterprise_audit_log` WRITE;
/*!40000 ALTER TABLE `enterprise_audit_log`
    DISABLE KEYS */;
INSERT INTO `enterprise_audit_log`
VALUES (1, 'd3bff26f-df28-434a-bd0b-0d376154010c', '/gateway/auth/oauth/token', 'POST', 200, 'anonymousUser',
        '2020-05-02 10:03:29', '2020-05-02 10:03:29'),
       (2, '2a8c9012-e2aa-4f44-931d-1c7984ca35f7', '/gateway/auth/oauth/token', 'POST', 200, 'anonymousUser',
        '2020-05-02 10:03:30', '2020-05-02 10:03:30');
/*!40000 ALTER TABLE `enterprise_audit_log`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise_user`
--

DROP TABLE IF EXISTS `enterprise_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_user`
(
    `user_id`             bigint(20)     NOT NULL AUTO_INCREMENT,
    `username`            varchar(32)    NOT NULL,
    `password`            varchar(64)    NOT NULL,
    `additional_info`     varchar(128)            DEFAULT '' COMMENT '附属的额外信息',
    `account_locked`      enum ('1','0') NOT NULL DEFAULT '0' COMMENT '1-被锁定，0-未锁定',
    `credentials_expired` enum ('1','0') NOT NULL DEFAULT '0' COMMENT '1-凭证过期，0-未过期',
    `account_enable`      enum ('0','1') NOT NULL COMMENT '账户是否启用, 默认0, 0-未启用,1-启用',
    `create_time`         timestamp      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time`         timestamp      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted`             tinyint(4)     NOT NULL DEFAULT '0',
    PRIMARY KEY (`user_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 9
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise_user`
--

LOCK TABLES `enterprise_user` WRITE;
/*!40000 ALTER TABLE `enterprise_user`
    DISABLE KEYS */;
INSERT INTO `enterprise_user`
VALUES (1, 'wangfulin', '$2a$10$ptzA9mlMutWXiTYtG7XGOOjv2zFsaiS32q/GPgqInXwQqPz.gffC2', 'info...', '0', '0', '1',
        '2020-04-25 03:53:41', '2020-04-25 09:09:58', 0),
       (2, 'meta@security.com', '$2a$10$NJ0CCo3OCWcNHMOafg9boel04N9sgDfUWa3.wsTk5vjo2SF.uwCZ.', 'info', '0', '0', '1',
        '2020-04-25 10:34:34', '2020-04-25 10:34:35', 0),
       (4, 'wangful15@outlook.com', '$2a$10$L.exQF9cotnVa6n8biogguAxDJq18iymO2pUFuEe7lWWHzFRt6Y26', 'additional', '0',
        '0', '1', '2020-05-01 16:21:42', '2020-05-01 16:21:42', 0),
       (7, 'mobile_user:1319101090032355', '', '', '0', '0', '1', '2020-05-02 00:46:19', '2020-05-02 00:46:19', 0),
       (8, 'mobile_user:18192800384', '', '', '0', '0', '1', '2020-05-02 00:53:33', '2020-05-02 00:53:33', 0);
/*!40000 ALTER TABLE `enterprise_user`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprise_user_additional`
--

DROP TABLE IF EXISTS `enterprise_user_additional`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enterprise_user_additional`
(
    `addition_id`   bigint(20)     NOT NULL AUTO_INCREMENT COMMENT '审计ID',
    `user_id`       bigint(20)     NOT NULL COMMENT '用户ID',
    `mobile`        varchar(16)    NOT NULL COMMENT '手机号',
    `gender`        enum ('M','F') NOT NULL COMMENT '性别',
    `addition_name` varchar(16)    NOT NULL COMMENT '系统应用名',
    `email`         varchar(32)    NOT NULL COMMENT '邮箱',
    `create_time`   datetime       NOT NULL COMMENT '创建时间',
    `update_time`   datetime       NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`addition_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 24
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprise_user_additional`
--

LOCK TABLES `enterprise_user_additional` WRITE;
/*!40000 ALTER TABLE `enterprise_user_additional`
    DISABLE KEYS */;
INSERT INTO `enterprise_user_additional`
VALUES (22, 7, '1319101090032355', 'M', '', '', '2020-05-02 08:46:19', '2020-05-02 08:46:19'),
       (23, 8, '18192800384', 'M', '', '', '2020-05-02 08:53:33', '2020-05-02 08:53:33');
/*!40000 ALTER TABLE `enterprise_user_additional`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_client_details`
--

DROP TABLE IF EXISTS `oauth_client_details`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_client_details`
(
    `client_id`              varchar(64)    NOT NULL COMMENT '客户端ID',
    `client_secret`          varchar(255)   NOT NULL COMMENT '客户端秘钥',
    `resource_ids`           varchar(128) DEFAULT '' COMMENT '令牌可访问的资源服务器ID（按 "," 分隔)',
    `grant_types`            varchar(128) DEFAULT '' COMMENT '授权模式 （按 "," 分隔)',
    `scopes`                 varchar(64)  DEFAULT '' COMMENT '授权作用域 （按 "," 分隔)',
    `server_redirect_uri`    varchar(255) DEFAULT '' COMMENT '客户端服务器注册的授权码回调地址',
    `access_token_validity`  int(11)      DEFAULT '0' COMMENT '访问令牌的有效期，s为单位',
    `refresh_token_validity` int(11)      DEFAULT '0' COMMENT '刷新令牌的有效期，s为单位',
    `additional_information` varchar(128) DEFAULT '' COMMENT '令牌附属的额外信息',
    `auto_approve`           enum ('0','1') NOT NULL COMMENT '默认0,适用于authorization_code模式,设置用户是否自动approval操作,设置 1 跳过用户确认授权操作页面，直接跳到redirect_uri',
    `created_at`             datetime       NOT NULL COMMENT '创建时间',
    `updated_at`             datetime       NOT NULL COMMENT '修改时间',
    `deleted`                int(11)      DEFAULT '0' COMMENT '数据删除标注 0 正常',
    PRIMARY KEY (`client_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_client_details`
--

LOCK TABLES `oauth_client_details` WRITE;
/*!40000 ALTER TABLE `oauth_client_details`
    DISABLE KEYS */;
INSERT INTO `oauth_client_details`
VALUES ('cooperation-id-5ead0d23f1cb4', '$2a$10$8hFiiSJZMVm7LnbRLLh.f.W//PyLv55Iac3InY.YqeNZ3GRO7FnWu',
        'gateway-resource,enterprise-service', 'password,refresh_token', 'read', '', 6000, 12000, '', '1',
        '2020-05-02 01:04:08', '2020-05-02 01:04:08', 0),
       ('cooperation-id-5ead0eb04f4e7', '$2a$10$hAHd4JAIZ/Kvsez/D6nUQ.qfXLV85W8TN0.D9Gm/Vpt3a7X7ZArFG',
        'gateway-resource,enterprise-owner', 'authorization_code,refresh_token,mobile_code', 'read,write',
        'http://localhost:8021/kaisa-group/auth', 6000, 12000, '', '1', '2020-05-02 01:10:50', '2020-05-02 01:10:50',
        0),
       ('gateway-client-0000', '$2a$10$BLsrwnez6X6oycqDnqnHd.toGNbkUDBE.X.XGe5f2f3mZaUz2XTyC', 'gateway-resource',
        'password,refresh_token', 'read', '', 6000, 12000, '', '1', '2020-05-02 01:07:14', '2020-05-02 01:07:14', 0);
/*!40000 ALTER TABLE `oauth_client_details`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_permission`
--

DROP TABLE IF EXISTS `oauth_permission`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_permission`
(
    `permission_id`          bigint(20)   NOT NULL AUTO_INCREMENT COMMENT '权限ID',
    `permission_group_id`    bigint(20)   NOT NULL COMMENT '权限组ID',
    `permission_group_name`  varchar(64)  NOT NULL COMMENT '权限组',
    `permission_description` varchar(128) NOT NULL COMMENT '权限描述',
    `permission_mask`        int(11)      NOT NULL COMMENT '权限标记位, 10进制存储, 标识为2进制',
    `created_at`             datetime     NOT NULL COMMENT '创建时间',
    `updated_at`             datetime     NOT NULL COMMENT '修改时间',
    `meta_flag`              int(11) DEFAULT '0' COMMENT '数据删除标注 0 正常',
    PRIMARY KEY (`permission_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_permission`
--

LOCK TABLES `oauth_permission` WRITE;
/*!40000 ALTER TABLE `oauth_permission`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_permission`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_permission_roles`
--

DROP TABLE IF EXISTS `oauth_permission_roles`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_permission_roles`
(
    `permission_role_id`     bigint(20)   NOT NULL AUTO_INCREMENT COMMENT '权限角色ID',
    `permission_description` varchar(128) NOT NULL COMMENT '权限描述',
    `permission_mask`        int(11)      NOT NULL COMMENT '权限标记位, 10进制存储, 标识为2进制',
    `created_at`             datetime     NOT NULL COMMENT '创建时间',
    `updated_at`             datetime     NOT NULL COMMENT '修改时间',
    `meta_flag`              int(11) DEFAULT '0' COMMENT '数据删除标注 0 正常',
    PRIMARY KEY (`permission_role_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_permission_roles`
--

LOCK TABLES `oauth_permission_roles` WRITE;
/*!40000 ALTER TABLE `oauth_permission_roles`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_permission_roles`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_user_permission_role`
--

DROP TABLE IF EXISTS `oauth_user_permission_role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_user_permission_role`
(
    `oauth_user_permission_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限角色用户ID',
    `user_id`                  bigint(20) NOT NULL COMMENT '权限描述',
    `oauth_permission_role_id` bigint(20) NOT NULL COMMENT '权限标记位, 10进制存储, 标识为2进制',
    `created_at`               datetime   NOT NULL COMMENT '创建时间',
    `updated_at`               datetime   NOT NULL COMMENT '修改时间',
    `meta_flag`                int(11) DEFAULT '0' COMMENT '数据删除标注 0 正常',
    PRIMARY KEY (`oauth_user_permission_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_user_permission_role`
--

LOCK TABLES `oauth_user_permission_role` WRITE;
/*!40000 ALTER TABLE `oauth_user_permission_role`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_user_permission_role`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-05-02 23:06:59