package club.example.security.entity;


import club.example.security.constant.EnumAccountEnable;
import club.example.security.constant.EnumAccountLocked;
import club.example.security.constant.EnumCredentialExpired;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
@TableName("enterprise_user")
public class EnterpriseUser {

    @TableId
    private Long userId;

    private String username;

    private String password;

    private String additionalInfo;

    private EnumAccountLocked accountLocked;

    private EnumCredentialExpired credentialsExpired;

    private EnumAccountEnable accountEnable;

    private Instant createTime;

    private Instant updateTime;

    private int deleted;
}
