package club.example.security.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
@ToString
public class MobileCode {

    @NotBlank(message = "手机号码不能为空")
    private String mobile;

    @NotBlank(message = "设备ID不能为空")
    private String deviceId;
}
