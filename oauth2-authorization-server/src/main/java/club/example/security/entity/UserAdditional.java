package club.example.security.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@TableName("enterprise_user_additional")
public class UserAdditional {

    @TableId
    private Long additionId;
    private Long userId;
    private String mobile;

    private String gender;

    private String additionName;

    private String email;

    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
