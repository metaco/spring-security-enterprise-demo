package club.example.security.service;


import club.example.security.constant.EnumAccountEnable;
import club.example.security.constant.EnumAccountLocked;
import club.example.security.entity.EnterpriseUser;
import club.example.security.repository.EnterpriseUserMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ApplicationUserDetailService implements UserDetailsService {

    private final EnterpriseUserMapper userMapper;

    @Autowired
    public ApplicationUserDetailService(EnterpriseUserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LambdaQueryWrapper<EnterpriseUser> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper
                .eq(EnterpriseUser::getUsername, username)
                .eq(EnterpriseUser::getDeleted, 0);
        EnterpriseUser enterpriseUser = userMapper.selectOne(lambdaQueryWrapper);
        if (null == enterpriseUser) {
            throw new UsernameNotFoundException("username = " + username);
        }

        return User.builder()
                .username(username)
                .password(enterpriseUser.getPassword())
                .authorities(AuthorityUtils.commaSeparatedStringToAuthorityList("admin"))
                .accountExpired(false)
                .accountLocked(enterpriseUser.getAccountLocked() == EnumAccountLocked.LOCKED)
                .disabled(enterpriseUser.getAccountEnable() == EnumAccountEnable.DISABLE)
                .build();
    }
}
