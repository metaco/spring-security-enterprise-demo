package club.example.security.service;

import club.example.security.constant.EnumAccountEnable;
import club.example.security.constant.EnumAccountLocked;
import club.example.security.constant.EnumCredentialExpired;
import club.example.security.entity.EnterpriseUser;
import club.example.security.entity.UserAdditional;
import club.example.security.exception.MobileCodeRateLimitException;
import club.example.security.exception.MobileCodeValidationException;
import club.example.security.provider.MobileCodeDataProvider;
import club.example.security.provider.MobileCodeTransferProvider;
import club.example.security.repository.EnterpriseUserMapper;
import club.example.security.repository.UserAdditionalMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class MobileCodeAuthenticationServiceImpl implements MobileCodeAuthenticationService {

    private final static String MOBILE_CODE_RATE_LIMIT_KEY = "mobile:rate:";
    private final static String MOBILE_CODE_DATA_KEY = "mobile:code:";
    private final static int MOBILE_CODE_RATE_LIMIT = 3;

    private final StringRedisTemplate stringRedisTemplate;

    private ValueOperations<String, String> valueOperations;

    private final UserAdditionalMapper userAdditionalMapper;
    private final EnterpriseUserMapper enterpriseUserMapper;

    private MobileCodeTransferProvider mobileCodeTransferProvider;

    private final  MobileCodeDataProvider mobileCodeDataProvider = new MobileCodeDataProvider();

    public MobileCodeAuthenticationServiceImpl(StringRedisTemplate stringRedisTemplate,
                                               UserAdditionalMapper userAdditionalMapper,
                                               EnterpriseUserMapper enterpriseUserMapper) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.userAdditionalMapper = userAdditionalMapper;
        this.enterpriseUserMapper = enterpriseUserMapper;
    }

    public void setMobileCodeTransferProvider(MobileCodeTransferProvider mobileCodeTransferProvider) {
        this.mobileCodeTransferProvider = mobileCodeTransferProvider;
    }

    @PostConstruct
    public void init() {
        valueOperations = stringRedisTemplate.opsForValue();
    }

    @Override
    public String createCode(String mobile) {
        log.info("createCode mobile = {}", mobile);

        String validateCode = valueOperations.get(MOBILE_CODE_DATA_KEY + mobile);
        if (StringUtils.hasText(validateCode)) {
            // 接入供应商
            return validateCode;
        }
        // 限流操作
        String rateLimitKey = MOBILE_CODE_RATE_LIMIT_KEY + mobile;
        String rateLimit = valueOperations.get(rateLimitKey);
        if (null != rateLimit && Integer.parseInt(rateLimit) >= MOBILE_CODE_RATE_LIMIT) {
            throw new MobileCodeRateLimitException("validation code sending exceed the limitation(3) ...#mobile#" + mobile);
        }

        registerMobileUser(mobile);
        // 发送校验码
        String code = mobileCodeDataProvider.provide();
        if (mobileCodeTransferProvider != null) {
            mobileCodeTransferProvider.transfer(mobile, code);
        }
        valueOperations.set(MOBILE_CODE_DATA_KEY + mobile, code, 300L, TimeUnit.SECONDS);
        // 计入发送计数器
        if (null != rateLimit) {
            valueOperations.increment(rateLimitKey);
        } else {
            valueOperations.set(rateLimitKey, String.valueOf(1), 86400L, TimeUnit.SECONDS);
        }
        log.info("mobile code : {} sending...", code);
        // TODO 接入平台供应商
        return code;
    }

    @Override
    public boolean authenticate(String mobile, String authenticationCode) {
        log.info("mobile = {}, authCode = {}", mobile, authenticationCode);
        String validateCode = valueOperations.get(MOBILE_CODE_DATA_KEY + mobile);
        if (!StringUtils.hasText(validateCode)) {
            throw new MobileCodeValidationException("mobile validation code has expired");
        }

        if (! authenticationCode.equals(validateCode)) {
            throw new MobileCodeValidationException("invalid mobile code");
        }
        stringRedisTemplate.expire(MOBILE_CODE_DATA_KEY + mobile, 1L, TimeUnit.SECONDS);
        return true;

    }

    private void registerMobileUser(String mobile) {
        Integer count = userAdditionalMapper.findByMobile(mobile);
        if (count == null || count == 0) {
            // 对不存在的手机号码，生成用户
            EnterpriseUser enterpriseUser = new EnterpriseUser();
            enterpriseUser.setUsername("mobile_user:" + mobile);
            enterpriseUser.setPassword("");
            enterpriseUser.setAdditionalInfo("");
            enterpriseUser.setAccountLocked(EnumAccountLocked.UNLOCKED);
            enterpriseUser.setCredentialsExpired(EnumCredentialExpired.NORMAL);
            enterpriseUser.setAccountEnable(EnumAccountEnable.ENABLE);
            enterpriseUser.setCreateTime(Instant.now());
            enterpriseUser.setUpdateTime(Instant.now());

            int insertedRow = enterpriseUserMapper.insert(enterpriseUser);

            if (insertedRow == 0) {
                throw new RuntimeException("注册默认手机号码用户失败");
            }

            createUserAdditional(mobile);
        }
    }

    private void createUserAdditional(String mobile) {
        LambdaQueryWrapper<EnterpriseUser> lambdaQueryWrapper = Wrappers.lambdaQuery(EnterpriseUser.class);
        lambdaQueryWrapper.eq(EnterpriseUser::getUsername,
                "mobile_user:" + mobile);
        EnterpriseUser enterpriseUser1 = enterpriseUserMapper.selectOne(lambdaQueryWrapper);
        if (null != enterpriseUser1) {
            UserAdditional userAdditional = new UserAdditional();
            userAdditional.setUserId(enterpriseUser1.getUserId());
            userAdditional.setMobile(mobile);
            userAdditional.setGender("M");
            userAdditional.setAdditionName("");
            userAdditional.setEmail("");
            userAdditional.setCreateTime(LocalDateTime.now());
            userAdditional.setUpdateTime(LocalDateTime.now());

            userAdditionalMapper.insert(userAdditional);
        }
    }
}
