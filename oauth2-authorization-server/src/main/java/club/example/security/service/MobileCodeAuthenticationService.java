package club.example.security.service;

public interface MobileCodeAuthenticationService {

    /**
     * 生成 短信校验码
     * @param mobile String
     * @return String
     */
    String createCode(String mobile);

    /**
     * 校验短信校验码
     * @param mobile String
     * @param authenticationCode String
     * @return boolean
     */
    boolean authenticate(String mobile, String authenticationCode);
}
