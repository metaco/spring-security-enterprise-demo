package club.example.security.service;

import club.example.security.entity.OAuthClientDetail;
import club.example.security.repository.OAuthClientDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Primary
@Service
public class ApplicationClientDetailsService implements ClientDetailsService {

    private final OAuthClientDetailMapper oAuthClientDetailMapper;

    public ApplicationClientDetailsService(OAuthClientDetailMapper oAuthClientDetailMapper) {
        this.oAuthClientDetailMapper = oAuthClientDetailMapper;
    }

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        log.info("loadClientByClientId clientId = {}", clientId);
        LambdaQueryWrapper<OAuthClientDetail> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper
                .eq(OAuthClientDetail::getDeleted, 0)
                .eq(OAuthClientDetail::getClientId, clientId);
        OAuthClientDetail authClientDetail = oAuthClientDetailMapper.selectOne(lambdaQueryWrapper);
        if (null == authClientDetail) {
            throw new ClientRegistrationException("no such clientId:" + clientId);
        }

        BaseClientDetails baseClientDetails = new BaseClientDetails();
        BeanUtils.copyProperties(authClientDetail, baseClientDetails);
        // 配置授权类型
        String detailGrantTypes = authClientDetail.getGrantTypes();
        if (StringUtils.hasText(detailGrantTypes)) {
            baseClientDetails.setAuthorizedGrantTypes(
                    Arrays.asList(detailGrantTypes.split(",")));
        }
        // 配置客户端访问作用域
        String detailScopes = authClientDetail.getScopes();
        if (StringUtils.hasText(detailScopes)) {
            baseClientDetails.setScope(Arrays.asList(detailScopes.split(",")));
        }
        // 配置可以访问的资源服务器
        String resourceIds = authClientDetail.getResourceIds();
        if (StringUtils.hasText(resourceIds)) {
            baseClientDetails.setResourceIds(Arrays.asList(resourceIds.split(",")));
        }
        // 配置授权回调发放授权码的注册地址，（当授权类型为 authorization_code 类型有效）
        String redirectUri = authClientDetail.getRedirectUri();
        if (StringUtils.hasText(redirectUri)) {
            Set<String> stringSet = Arrays.stream(redirectUri.split(","))
                    .collect(Collectors.toSet());
            baseClientDetails.setRegisteredRedirectUri(stringSet);
        }

        // 设置 AutoApprove 会影响 oauth/token_key 接口的访问权限
        baseClientDetails.setAutoApproveScopes(
                authClientDetail.getAutoApprove().isApprove() ? Collections.singleton("true") : Collections.emptyList());
        baseClientDetails.setAuthorities(Collections.emptyList());
        log.info("load clientDetail = {}", baseClientDetails);
        //自动批准作用于，授权码模式时使用，登录验证后直接返回code，不再需要下一步点击授权
        return baseClientDetails;
    }
}
