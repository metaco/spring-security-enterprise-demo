package club.example.security.provider;

import club.example.security.service.MobileCodeAuthenticationService;
import club.example.security.token.MobileCodeAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

@Component
public class MobileCodeAuthenticationProvider implements AuthenticationProvider {

    // 短信验证码校验器 提供 验证码校验功能
    // 查询用户手机号码，并注册手机号

    private final MobileCodeAuthenticationService mobileCodeAuthenticationService;

    public MobileCodeAuthenticationProvider(MobileCodeAuthenticationService mobileCodeAuthenticationService) {
        this.mobileCodeAuthenticationService = mobileCodeAuthenticationService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        MobileCodeAuthenticationToken mobileToken = (MobileCodeAuthenticationToken) authentication;
        String mobile = (String ) mobileToken.getPrincipal();
        String code = (String) mobileToken.getCredentials();
        if (mobileCodeAuthenticationService.authenticate(mobile, code)) {
            mobileToken.setDetails(authentication.getDetails());
            return new MobileCodeAuthenticationToken(mobileToken.getPrincipal(), mobileToken.getCredentials(),
                    AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_ADMIN"));
        }
        return mobileToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return MobileCodeAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
