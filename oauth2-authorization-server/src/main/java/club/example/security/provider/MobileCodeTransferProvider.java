package club.example.security.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 短信发送供应商
 */
public interface MobileCodeTransferProvider {

    /**
     * 发送短信
     * @param mobile String
     * @param validationCode String
     */
    void transfer(String mobile, String validationCode);
}
