package club.example.security.provider;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MobileCodeDataProvider {

    private final static int MOBILE_CODE_LENGTH = 6;

    private final static String[] seeds = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    public String provide() {
        List<String> seedList = Arrays.asList(seeds);
        Collections.shuffle(seedList);
        List<String> codeList = seedList.subList(0, MOBILE_CODE_LENGTH - 1);
        return String.join("", codeList);
    }
}
