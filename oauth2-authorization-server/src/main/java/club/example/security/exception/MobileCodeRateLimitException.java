package club.example.security.exception;

public class MobileCodeRateLimitException extends ApplicationAuthenticationException {

    public MobileCodeRateLimitException(String message) {
        super(message);
    }
}
