package club.example.security.exception;

import org.springframework.security.core.AuthenticationException;

public class MobilePhoneValidationException extends ApplicationAuthenticationException {
    public MobilePhoneValidationException(String message) {
        super(message);
    }
}
