package club.example.security.exception;

import org.springframework.security.core.AuthenticationException;

public class MobileCodeValidationException extends AuthenticationException {
    public MobileCodeValidationException(String message) {
        super(message);
    }
}
