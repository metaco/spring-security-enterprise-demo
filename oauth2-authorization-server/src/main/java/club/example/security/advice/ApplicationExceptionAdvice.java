package club.example.security.advice;

import club.example.core.JsonResponse;
import club.example.security.exception.ApplicationAuthenticationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;

@Slf4j
@RestControllerAdvice
public class ApplicationExceptionAdvice {

    @ExceptionHandler(ApplicationAuthenticationException.class)
    public JsonResponse<String> processingAppException(ApplicationAuthenticationException exception) {
        log.info("processingAppException ex = {}", Arrays.toString(exception.getStackTrace()));
        return new JsonResponse<>(-10001, exception.getMessage());
    }
}
