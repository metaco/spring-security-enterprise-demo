package club.example.security.endpoint;

import club.example.core.JsonResponse;
import club.example.core.validator.MobileCodeValidator;
import club.example.security.entity.MobileCode;
import club.example.security.exception.MobileCodeValidationException;
import club.example.security.exception.MobilePhoneValidationException;
import club.example.security.service.MobileCodeAuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/sms")
public class MobileCodeAuthenticationEndpoint {

    private final MobileCodeValidator validator = new MobileCodeValidator();

    // 短信验证码 服务
    private final MobileCodeAuthenticationService mobileCodeAuthService;

    public MobileCodeAuthenticationEndpoint(MobileCodeAuthenticationService mobileCodeAuthService) {
        this.mobileCodeAuthService = mobileCodeAuthService;
    }

    @PostMapping
    public JsonResponse<String> sendCode(@RequestBody @Valid MobileCode mobileCode) {
        log.info("sendCode... mobile = {}", mobileCode.toString());
        String mobile = mobileCode.getMobile();
        if (! validator.validate(mobile)) {
            throw new MobilePhoneValidationException("invalid mobile phone number");
        }
        String code = mobileCodeAuthService.createCode(mobile);

        return new JsonResponse<>(0, "success sending...", mobile + "_" + code);
    }
}
