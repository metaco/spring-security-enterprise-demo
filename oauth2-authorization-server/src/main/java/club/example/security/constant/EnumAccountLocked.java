package club.example.security.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;

public enum EnumAccountLocked {
    LOCKED("1", "锁定"),
    UNLOCKED("0", "未锁定");

    @EnumValue
    private final String value;

    private final String description;

    EnumAccountLocked(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
