package club.example.security.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;

public enum EnumAccountEnable {
    ENABLE("1", "启用"),
    DISABLE("0", "禁用");

    @EnumValue
    private final String value;

    private final String description;

    EnumAccountEnable(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
