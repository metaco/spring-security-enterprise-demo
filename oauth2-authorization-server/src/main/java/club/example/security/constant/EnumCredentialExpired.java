package club.example.security.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;

public enum EnumCredentialExpired {

    EXPIRED("1", "过期"),
    NORMAL("0", "未过期");

    @EnumValue
    private final String value;

    private final String description;

    EnumCredentialExpired(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
