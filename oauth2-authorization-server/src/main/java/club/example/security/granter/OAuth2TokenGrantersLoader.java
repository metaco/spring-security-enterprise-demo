package club.example.security.granter;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OAuth2TokenGrantersLoader {

    private AuthorizationServerEndpointsConfigurer endpointsConfigurer;

    private AuthenticationManager authenticationManager;

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public void setEndpointsConfigurer(AuthorizationServerEndpointsConfigurer endpointsConfigurer) {
        this.endpointsConfigurer = endpointsConfigurer;
    }

    public List<TokenGranter> load() {
        if (null == endpointsConfigurer || null == authenticationManager) {
            return Collections.emptyList();
        }

        List<TokenGranter> tokenGranters = new ArrayList<>();
        tokenGranters.add(new AuthorizationCodeTokenGranter(
                endpointsConfigurer.getTokenServices(),
                endpointsConfigurer.getAuthorizationCodeServices(), endpointsConfigurer.getClientDetailsService(),
                endpointsConfigurer.getOAuth2RequestFactory()));

        tokenGranters.add(new RefreshTokenGranter(
                endpointsConfigurer.getTokenServices(),
                endpointsConfigurer.getClientDetailsService(),
                endpointsConfigurer.getOAuth2RequestFactory()));

        tokenGranters.add(new ResourceOwnerPasswordTokenGranter(
                authenticationManager, endpointsConfigurer.getTokenServices(),
                endpointsConfigurer.getClientDetailsService(),
                endpointsConfigurer.getOAuth2RequestFactory()));

        tokenGranters.add(new MobileCodeTokenGranter(authenticationManager,
                endpointsConfigurer.getTokenServices(), endpointsConfigurer.getClientDetailsService(),
                endpointsConfigurer.getOAuth2RequestFactory()));

        return tokenGranters;
    }
}
