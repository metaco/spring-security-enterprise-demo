package club.example.security.configure;

import club.example.security.provider.MobileCodeAuthenticationProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class ApplicationWebSecurityConfigure extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    private final MobileCodeAuthenticationProvider mobileCodeAuthenticationProvider;

    public ApplicationWebSecurityConfigure(
            @Qualifier("applicationUserDetailService") UserDetailsService userDetailsService,
            MobileCodeAuthenticationProvider mobileCodeAuthenticationProvider) {
        this.userDetailsService = userDetailsService;
        this.mobileCodeAuthenticationProvider = mobileCodeAuthenticationProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // 构建 AuthenticationManager
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(mobileCodeAuthenticationProvider)
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/sms", "/oauth/**").permitAll()
                .anyRequest().authenticated();
    }
}
