package club.example.security.configure.properties;

public class OAuth2Properties {

    private String jwtSignKey;

    private String sslKeyCertificate;

    private String sslKeyPhrase;

    private String sslKeyPair;

    public String getJwtSignKey() {
        return jwtSignKey;
    }

    public void setJwtSignKey(String jwtSignKey) {
        this.jwtSignKey = jwtSignKey;
    }

    public String getSslKeyCertificate() {
        return sslKeyCertificate;
    }

    public void setSslKeyCertificate(String sslKeyCertificate) {
        this.sslKeyCertificate = sslKeyCertificate;
    }

    public String getSslKeyPhrase() {
        return sslKeyPhrase;
    }

    public void setSslKeyPhrase(String sslKeyPhrase) {
        this.sslKeyPhrase = sslKeyPhrase;
    }

    public String getSslKeyPair() {
        return sslKeyPair;
    }

    public void setSslKeyPair(String sslKeyPair) {
        this.sslKeyPair = sslKeyPair;
    }
}
