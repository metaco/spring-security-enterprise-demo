package club.example.security.configure;

import club.example.security.configure.properties.EnterpriseSecurityProperties;
import club.example.security.granter.OAuth2TokenGrantersLoader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@Configuration
@EnableAuthorizationServer
@EnableConfigurationProperties(EnterpriseSecurityProperties.class)
public class AuthorizationServerConfigure extends AuthorizationServerConfigurerAdapter implements InitializingBean {

    private final EnterpriseSecurityProperties securityProperties;

    private final ClientDetailsService clientDetailsService;

    private final AuthenticationManager authenticationManager;

    private final OAuth2TokenGrantersLoader grantersLoader = new OAuth2TokenGrantersLoader();

    public AuthorizationServerConfigure(EnterpriseSecurityProperties securityProperties,
                                        ClientDetailsService clientDetailsService,
                                        AuthenticationManager authenticationManager) {
        this.securityProperties = securityProperties;
        this.clientDetailsService = clientDetailsService;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        grantersLoader.setAuthenticationManager(authenticationManager);
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtTokenEnhancer());
    }

    // 需要暴露 jwtTokenEnhancer 才有 oauth/token_key 接口
    @Bean
    public JwtAccessTokenConverter jwtTokenEnhancer() {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        // jwtAccessTokenConverter.setSigningKey("enterprise-key");
        String certificatePasscode = securityProperties.getOauth2().getSslKeyPhrase();
        // 密钥对信息数据
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(
                new ClassPathResource(securityProperties.getOauth2().getSslKeyCertificate()),
                certificatePasscode.toCharArray()
        );
        jwtAccessTokenConverter.setKeyPair(keyStoreKeyFactory.getKeyPair(
                securityProperties.getOauth2().getSslKeyPair()));
        return jwtAccessTokenConverter;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientDetailsService);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        grantersLoader.setEndpointsConfigurer(endpoints);

        endpoints
                .tokenStore(tokenStore())
                .tokenEnhancer(jwtTokenEnhancer())
                .tokenGranter(new CompositeTokenGranter(grantersLoader.load()))
                .authenticationManager(authenticationManager);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("isAuthenticated()")
                .checkTokenAccess("isAuthenticated()");
    }
}
