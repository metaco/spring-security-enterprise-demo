package club.example.security.configure.properties;

public class BrowserProperties {

    private String loginPage = "default-sign.html";

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }
}
