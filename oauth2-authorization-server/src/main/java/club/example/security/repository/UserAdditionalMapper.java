package club.example.security.repository;

import club.example.security.entity.UserAdditional;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserAdditionalMapper extends BaseMapper<UserAdditional> {

    Integer findByMobile(@Param("mobile") String mobile);
}
