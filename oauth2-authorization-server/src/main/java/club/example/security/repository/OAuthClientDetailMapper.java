package club.example.security.repository;


import club.example.security.entity.OAuthClientDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OAuthClientDetailMapper extends BaseMapper<OAuthClientDetail> {
}
