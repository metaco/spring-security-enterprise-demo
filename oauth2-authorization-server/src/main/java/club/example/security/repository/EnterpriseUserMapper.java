package club.example.security.repository;


import club.example.security.entity.EnterpriseUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EnterpriseUserMapper extends BaseMapper<EnterpriseUser> {
}
