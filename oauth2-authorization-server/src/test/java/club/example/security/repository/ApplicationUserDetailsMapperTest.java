package club.example.security.repository;
import club.example.security.constant.EnumAccountLocked;
import club.example.security.constant.EnumCredentialExpired;
import java.time.Instant;
import club.example.security.constant.EnumAccountEnable;

import club.example.security.entity.EnterpriseUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class ApplicationUserDetailsMapperTest {

    @Autowired
    private EnterpriseUserMapper enterpriseUserMapper;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void testStoreUser() {
        EnterpriseUser enterpriseUser = new EnterpriseUser();

        enterpriseUser.setUsername("wangful15@outlook.com");
        enterpriseUser.setPassword(passwordEncoder.encode("passworda1!"));
        enterpriseUser.setAdditionalInfo("additional");
        enterpriseUser.setAccountLocked(EnumAccountLocked.UNLOCKED);
        enterpriseUser.setCredentialsExpired(EnumCredentialExpired.NORMAL);
        enterpriseUser.setAccountEnable(EnumAccountEnable.ENABLE);
        enterpriseUser.setCreateTime(Instant.now());
        enterpriseUser.setUpdateTime(Instant.now());
        enterpriseUser.setDeleted(0);

        enterpriseUserMapper.insert(enterpriseUser);
    }
}
