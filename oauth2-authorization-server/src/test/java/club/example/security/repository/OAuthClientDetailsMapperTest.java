package club.example.security.repository;

import club.example.security.entity.OAuthClientDetail;
import club.example.security.entity.OAuthClientDetail.EnumAutoApprove;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;

@SpringBootTest
public class OAuthClientDetailsMapperTest {

    @Autowired
    private OAuthClientDetailMapper oAuthClientDetailMapper;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void testOAuthClientStore() {
        OAuthClientDetail authClientDetail = new OAuthClientDetail();
        authClientDetail.setClientId("cooperation-id-5ead0eb04f4e7");
        authClientDetail.setClientSecret(passwordEncoder.encode("5ead0eb6a5ac9")); // 54  v53mg8ch0n    55 3kb8rj9b2nb84
        authClientDetail.setResourceIds("enterprise-owner");
        authClientDetail.setRedirectUri("");
        authClientDetail.setGrantTypes("authorization_code,refresh_token");
        authClientDetail.setScopes("read,write");
        authClientDetail.setAutoApprove(EnumAutoApprove.ENABLE_AUTO);
        authClientDetail.setAccessTokenValiditySeconds(6000);
        authClientDetail.setRefreshTokenValiditySeconds(12000);
        authClientDetail.setCreatedAt(LocalDateTime.now());
        authClientDetail.setUpdatedAt(LocalDateTime.now());
        authClientDetail.setDeleted(0);

        oAuthClientDetailMapper.insert(authClientDetail);
    }
}
