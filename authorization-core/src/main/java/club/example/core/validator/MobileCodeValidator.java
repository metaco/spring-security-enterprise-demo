package club.example.core.validator;

import org.springframework.util.StringUtils;

public class MobileCodeValidator {

    private final static int MOBILE_PHONE_LENGTH_CN = 11;

    private final static String REGULAR_EXPRESSION = "^[1][3,4,5,7,8][0-9]{9}$";

    public boolean validate(String mobile) {
        return StringUtils.hasLength(mobile)
               && mobile.length() == MOBILE_PHONE_LENGTH_CN
               && mobile.matches(REGULAR_EXPRESSION);
    }
}
