package club.example.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class AuthenticationToken {

    private boolean active;

    @JsonProperty("client_id")
    private String clientId;

    private String[] scope;

    @JsonProperty("user_name")
    private String username;

    private String[] aud;

    private Date exp;

    private String[] authorities;
}
